 %%------------------------Help------------------------

openHelp:-
	writeTabnl("help - Show this page"),
	writeTabnl("menu - Show main menu"),
	writeTabnl("exit - Close program"),
	readMenuitem(_MenuItem),
	!, openHelp.


%%------------------------0 : Main menu------------------------

openMainMenu:-
	writeTabnl("Furniture store"),
	writeTabnl("1 - Show all furniture pieces"),
	writeTabnl("2 - Show all components"),
	writeTabnl("3 - Show furniture price list"),
	writeTabnl("4 - Open furniture constructor"),
	writeTabnl("5 - Find furniture by parameters"),
	writeTabnl("6 - Find furniture by price category"),
	readMenuitem(MenuItem),
	mainMenu(MenuItem),
	openMainMenu.

mainMenu("1"):- !, openAllFurnituresMenu, showMainMenuAlert, openMainMenu.
mainMenu("2"):- !, openAllPartsMenu, showMainMenuAlert, openMainMenu.
mainMenu("3"):- !, openPriceListMenu, showMainMenuAlert, openMainMenu.
mainMenu("4"):- !, openConstructorMenu, showMainMenuAlert, openMainMenu.
mainMenu("5"):- !, openFindFurnituresByParamMenu, showMainMenuAlert, openMainMenu.
mainMenu("6"):- !, openFindFurnituresByPriceCategory, showMainMenuAlert, openMainMenu.
mainMenu(_):- showWrongMenuItemError.


%%------------------------1 : All Furnitures------------------------

openAllFurnituresMenu:-
	writeTabnl("1 - Without sorting"),
	writeTabnl("2 - Sort by price"),
	readMenuitem(MenuItem), 
	allFurnituresMenu(MenuItem).

allFurnituresMenu("1"):- !, showFurnituresNoSorting.
allFurnituresMenu("2"):- !, showFurnituresSortByPrice.
allFurnituresMenu(_):- !, showWrongMenuItemError, openAllFurnituresMenu.


showFurnituresNoSorting:- 
	furniture(ID, Name, PartGroups, Price, Params),
	Furniture = furniture(ID, Name, PartGroups, Price, Params),
	showFurniture(Furniture), 
	fail.
showFurnituresNoSorting.


showFurnituresSortByPrice:- 
	findall(Pair, buildFurniturePair(Pair), Pairs),
	sortPairs(Pairs, Sorted),
	unwrapPairs(Sorted, [], Ids),
	openFurnituresSortTypeMenu(Ids),
	fail.
showFurnituresSortByPrice.


openFurnituresSortTypeMenu(Ids):-
	writeTabnl("1 - Ascending"),
	writeTabnl("2 - Descending"),
	readMenuitem(MenuItem), 
	furnituresSortTypeMenu(MenuItem, Ids).

furnituresSortTypeMenu("1", Ids):- !, showFurnituresById(Ids).
furnituresSortTypeMenu("2", Ids):- !, reverse(Ids, Reversed), showFurnituresById(Reversed).
furnituresSortTypeMenu(_, Ids):- !, showWrongMenuItemError, openFurnituresSortTypeMenu(Ids).


buildFurniturePair(Result):-
	furniture(ID, _, _, Price, _),
	Result = pair(ID, Price).

%%------------------------2 : All Parts------------------------

openAllPartsMenu:-
	writeTabnl("1 - Without sorting"),
	writeTabnl("2 - Sort by price"),
	readMenuitem(MenuItem), 
	allPartsMenu(MenuItem).

allPartsMenu("1"):- !, showPartsNoSorting.
allPartsMenu("2"):- !, showPartsSortByPrice.
allPartsMenu(_):- !, showWrongMenuItemError, openAllPartsMenu.

showPartsNoSorting:-
	part(ID, Name, Cost),
	showPart(part(ID, Name, Cost)),
	fail.
showPartsNoSorting.


showPartsSortByPrice:-
	findall(Pair, buildPartPair(Pair), Pairs),
	sortPairs(Pairs, Sorted),
	unwrapPairs(Sorted, [], Ids),
	openPartsSortTypeMenu(Ids),
	fail.
showPartsSortByPrice.

openPartsSortTypeMenu(Ids):-
	writeTabnl("1 - Ascending"),
	writeTabnl("2 - Descending"),
	readMenuitem(MenuItem), 
	partsSortTypeMenu(MenuItem, Ids).

partsSortTypeMenu("1", Ids):- !, reverse(Ids, Reversed), showPartsById(Reversed).
partsSortTypeMenu("2", Ids):- !, showPartsById(Ids).
partsSortTypeMenu(_, Ids):- !, showWrongMenuItemError, openPartsSortTypeMenu(Ids).

buildPartPair(Result):-
	part(ID, _, Cost),
	Result = pair(ID, Cost).

%%------------------------3 : Price List------------------------

openPriceListMenu:-
	writeTab("Type the name of the furniture: "),
	readStringIgnoreCase(Name), 
	priceListMenu(Name)
.

priceListMenu(SearchName):-
	findall(F, findFurnitureByNameLike(SearchName, F), Furnitures),
	length(Furnitures, Length),
	Length > 0,
	showFurnituresOrdered(Furnitures),
	writeTab("Type the furniture number: "),
	priceListMenuItem(Furnitures)
.

priceListMenu(_):- 
	writeTabnl("Furniture is not found, try again"), 
	readStringIgnoreCase(Name), 
	priceListMenu(Name)
.


priceListMenuItem(Furnitures):-
	readTerm(Read),
	number_string(Number, Read),

	length(Furnitures, Length),
	Index is Number - 1,
	Index =< Length,

	nth0(Index, Furnitures, Furniture),
	showFurnitureCost(Furniture)
.
priceListMenuItem(Furnitures):- writeTab("Type a valid number: "), priceListMenuItem(Furnitures).

findFurnitureByNameLike(SearchName, F):-
	furniture(ID, Name, Groups, Price, Params),
	string_lower(Name, NameLower),
	subString(NameLower, SearchName),
	F = furniture(ID, Name, Groups, Price, Params)
.


%%------------------------4 : Furniture Constructor ------------------------

openConstructorMenu:-
	writeTabnl("Furniture constructor"),
	writeTab("Type the furniture name: "),
	readStringQuoted(Name),

	buildPartGroups(PartGroups),
	buildParams(Params),

	writeTab("Type the price: "),
	readInt(Price),

	findall(ID, furniture(ID, _, _, _, _), FurnituresIds),
	max_list(FurnituresIds, MaxID),
	ID is MaxID + 1,

	insert(furniture(ID, Name, PartGroups, Price, Params))
.

buildParams(Params):- buildParams(0, [], Params).

buildParams(1, [_|T], T):-!.
buildParams(0, Stack, Params):-
	openParamsConstructorMenu(Param, Return),
	buildParams(Return, [Param|Stack], Temp),
	Params = Temp
.

openParamsConstructorMenu(Temp, Return):-
	writeTabnl("Parameters"),
	writeTabnl("1 - add a new parameter"),
	writeTabnl("2 - apply"),
	readSubMenuitem(MenuItem),
	paramsConstructorMenu(MenuItem, Temp, Return)
.

paramsConstructorMenu("1", Temp, 0):- paramsConstructor(Temp).
paramsConstructorMenu("2", _, 1):-!.

paramsConstructor(Temp):-
	writeTab("Type the name: "),
	readStringQuoted(Name),

	writeTab("Type the value: "),
	readStringQuoted(Value),
	Temp = param(Name, Value)
.

buildPartGroups(PartGroups):- buildPartGroups(0, [], PartGroups).

buildPartGroups(1, [_|T], T):-!.
buildPartGroups(0, Stack, PartGroups):-
	openPartGroupsConstructorMenu(Group, Return),
	buildPartGroups(Return, [Group|Stack], Temp),
	PartGroups = Temp
.

openPartGroupsConstructorMenu(Temp, Return):-
	writeTabnl("Components"),
	writeTabnl("1 - add existing component"),
	writeTabnl("2 - create a new component"),
	writeTabnl("3 - apply"),
	readSubMenuitem(MenuItem),
	partGroupsConstructorMenu(MenuItem, Temp, Return)
.

partGroupsConstructorMenu("1", Temp, 0):- partGroupsConstructor(Temp).
partGroupsConstructorMenu("3", _, 1):-!.

partGroupsConstructor(Temp):-
	writeTab("Search a component: "),
	readStringIgnoreCase(SearchName),
	findall(P, findPartByNameLike(SearchName, P), Parts),
	length(Parts, Length),
	Length > 0,
	showPartsOrdered(Parts),
	writeTab("Component index: "),
	partGroupsMenuItem(Parts, part(PartID, _, _)),
	writeTab("Component count: "),
	readInt(Count),
	Temp = group(PartID, Count)
.
partGroupsConstructor(_).

findPartByNameLike(SearchName, P):-
	part(ID, Name, Cost),
	string_lower(Name, NameLower),
	subString(NameLower, SearchName),
	P = part(ID, Name, Cost)
.



partGroupsMenuItem(Parts, Result):-
	readTerm(Read),
	number_string(Number, Read),

	length(Parts, Length),
	Index is Number - 1,
	Index =< Length,

	nth0(Index, Parts, Result),
	!
.
partGroupsMenuItem(Parts, Result):- writeTab("Type a valid number: "), partGroupsMenuItem(Parts, Result).


%%------------------------5 : Find furniture by parameters ------------------------

openFindFurnituresByParamMenu:-
	writeTab("Type the name of the parameter: "),
	readStringIgnoreCase(ParamSearchName), 
	findParamName(ParamSearchName, ParamName),

	writeTab("Type the value of the parameter: "),
	readString(ParamValue),
	findFurnitureByParam(param(ParamName, ParamValue), Furnitures),

	showFurnitures(Furnitures)
.
openFindFurnituresByParamMenu:- writeTab("Nothing found").

findParamName(SearchName, Name):-
	furniture(_, _, _, _, Params),
	hasParamIgnoreCase(SearchName, Params, Name),
	!
.

findParamName(SearchName, Name):-
	findAllParamsLike(SearchName, ParamsDup),
	findall(Name, member(param(Name, _), ParamsDup), NamesDup),
	sort(NamesDup, Names),
	length(Names, Length),
	Length > 0,
	writeTabnl("Did you meant one of these?"),
	showStringsOrdered(Names),
	writeTab("Type the parameter number: "),
	findParamNameMenuItem(Names, Name),
	!
.

findParamName(_, _):- 
	writeTabnl("Parameter is not found"),
	fail
.

findAllParamsLike(SearchName, Result):-
	findall(Params, findAllParamsLikeInner(SearchName, Params), ParamsList),
	flatMap(ParamsList, Result)
.

findAllParamsLikeInner(SearchName, Result):-
	furniture(_, _, _, _, Params),
	findall(P, findParamLike(SearchName, Params, P), Result)
.

flatMap([], []).
flatMap([List|T], Result):-
	flatMap(T, Temp),
	append(List, Temp, Result)
.

findParamNameMenuItem(Names, Name):-
	readTerm(Read),
	number_string(Number, Read),

	length(Names, Length),
	Index is Number - 1,
	Index =< Length,

	nth0(Index, Names, Name)
.

findParamNameMenuItem(Names, Name):- 
	writeTab("Type a valid number: "), 
	findParamNameMenuItem(Names, Name)
.


findFurnitureByParam(Param, Result):-
	furniture(ID, Name, Groups, Price, Params),
	member(Param, Params),
	findall(Furniture, Furniture = furniture(ID, Name, Groups, Price, Params), Result)
.


hasParamIgnoreCase(_, [], _):- fail.
hasParamIgnoreCase(ExactName, [param(Param, _)|T]):-
	string_lower(Param, ParamLower),
	string_lower(ExactName, ExactLower),
	ParamLower = ExactLower,
	hasParamIgnoreCase(ExactName, T)
.

findParamLike(_, [], _):- fail.
findParamLike(SearchName, [param(Name, Value)|_], P):-
	string_lower(SearchName, SearchLower),
	string_lower(Name, NameLower),
	subString(NameLower, SearchLower),
	P = param(Name, Value)
.
findParamLike(SearchName, [_|T], P):- findParamLike(SearchName, T, P).


%%------------------------6 : Find furniture by price category ------------------------

openFindFurnituresByPriceCategory:-
	writeTab("Type min price: "),
	readPositive(MinPrice, -1),
	writeTab("Type max price: "),
	readPositive(MaxPrice, -1),

	findFurnituresByPrice(MinPrice, MaxPrice, Furnitures),
	(
		Furnitures = [], writeTab("Nothing found");
		showFurnitures(Furnitures)
	)
.
openFindFurnituresByPriceCategory:- writeTab("Nothing found").

findFurnituresByPrice(MinPrice, MaxPrice, Result):-
	findall(F, getFurnitureByPrices(F, MinPrice, MaxPrice), Result).

getFurnitureByPrices(Furniture, MinPrice, MaxPrice):-
	furniture(ID, Name, PartGroups, Price, Params),
	(MaxPrice = -1; MaxPrice \= -1, Price =< MaxPrice),
	(MinPrice = -1; MinPrice \= -1, Price >= MinPrice),
	Furniture = furniture(ID, Name, PartGroups, Price, Params)
.

%%------------------------UI------------------------

showFurnituresById([]):-!.
showFurnituresById([ID|T]):-
	furniture(ID, Name, PartGroups, Price, Params),
	Furniture = furniture(ID, Name, PartGroups, Price, Params),
	showFurniture(Furniture),
	showFurnituresById(T)
.

showFurnituresCompact([]):-!.
showFurnituresCompact([Furniture|T]):-
	showFurnitureCompact(Furniture),
	showFurnituresCompact(T)
.

showFurnitureCompact(furniture(ID, Name, _PartGroups, Price, _Params)):-
	writeTab("("), write(ID), write(")  -  "), 
	write(Name), 
	writeTab("["),	write(Price), write(" rub]"),
	nl
.

showPartsOrdered(Parts):- showPartsOrdered(Parts, 1).
showPartsOrdered([],  _):-!.
showPartsOrdered([Part|T], Number):-
	showPartOrdered(Part, Number),
	NumberInc is Number + 1,
	showPartsOrdered(T, NumberInc)
.

showPartOrdered(part(_ID, Name, Cost), Number):-
	writeTab(Number), write(" - "), 
	write(Name), 
	write(" ["), write(Cost), write(" rub]"),
	nl
.

showFurnituresOrdered(Furnitures):- showFurnituresOrdered(Furnitures, 1).
showFurnituresOrdered([],  _):-!.
showFurnituresOrdered([Furniture|T], Number):-
	showFurnitureOrdered(Furniture, Number),
	NumberInc is Number + 1,
	showFurnituresOrdered(T, NumberInc)
.


showStringsOrdered(Strings):- showStringsOrdered(Strings, 1).
showStringsOrdered([],  _):-!.
showStringsOrdered([String|T], Number):-
	showStringOrdered(String, Number),
	NumberInc is Number + 1,
	showStringsOrdered(T, NumberInc)
.

showStringOrdered(String, Number):-
	writeTab(Number), write(" - "), 
	write(String),
	nl
.

showFurnitureOrdered(furniture(_ID, Name, _PartGroups, _Price, _Params), Number):-
	writeTab(Number), write(" - "), 
	write(Name), 
	nl
.

showFurnitureCost(furniture(_, Name, PartGroups, _, _)):-
	getFullCost(PartGroups, TotalCost),
	writeTab(Name), write("  -  "), write(TotalCost), write(" rub"), nl,
	writeTabnl("Components"),
	showPartsCost(PartGroups)
.	

getFullCost(PartGroups, Result):- getFullCost(PartGroups, Result, 0).
getFullCost([], Total, Total):-!.
getFullCost([group(PartID, Count)|T], Result, Total):-
	part(PartID, _, Cost),
	CostSum is Count * Cost,
	TotalInc is CostSum + Total,
	getFullCost(T, Result, TotalInc)
.

showPartsCost([]):-!.
showPartsCost([group(PartID, Count)|T]):-
	part(PartID, Name, Cost),
	CostSum is Count * Cost,
	writeTab(2, Name), 
	write(" (x"), write(Count), write(")  -  "), 
	write(CostSum), write(" rub"), nl,
	showPartsCost(T)
.

showFurniture(furniture(ID, Name, PartGroups, Price, Params)):-
	writeTab("("), write(ID), write(")  -  "), write(Name), 
	writeTab("["), write(Price), write(" rub]"), nl,
	writeTab("Parameters"), nl,
	showParams(Params),
	writeTab("Components"), nl,
	showPartGroups(PartGroups), nl.

showParams([]):-!.
showParams([param(Name, Value)|T]):-
	writeTab(2, Name), write(": "), write(Value), nl,
	showParams(T).

showPartGroups([]):-!.
showPartGroups([group(PartID, Count)|T]):-
	part(PartID, PartName, _),
	writeTab(2, PartName), write(" x "), write(Count), nl,
	showPartGroups(T).


showPart(part(ID, Name, Cost)):-
	writeTab("("), write(ID), write(")  -  "), write(Name), writeTab("["), write(Cost), write(" rub]"), nl.

showPartsById([]):-!.
showPartsById([ID|T]):-
	part(ID, Name, Cost),
	showPart(part(ID, Name, Cost)),
	showPartsById(T).

showFurnitures([]):-!.
showFurnitures([F|T]):-
	showFurniture(F),
	showFurnitures(T)
.

%%------------------------Utils------------------------

sortPairs(Pairs, Sorted):- sortPairs(Pairs, [], Sorted).
sortPairs([], Accum, Accum).
sortPairs([pair(Id, Value) | T], Acc, Sorted):- 
	sortPairs(pair(Id, Value), T, NT, pair(IdMax, Max)),
	sortPairs(NT, [pair(IdMax, Max) | Acc], Sorted).

sortPairs(pair(Id, X), [], [], pair(Id, X)).
sortPairs(pair(Id, X), [pair(IdY, Y)|T], [pair(IdY, Y)|NT], Max):- 
	X>Y, 
	sortPairs(pair(Id, X), T, NT, Max).
sortPairs(pair(Id, X), [pair(IdY, Y)|T], [pair(Id, X)|NT], Max):- 
	X=<Y, 
	sortPairs(pair(IdY, Y), T, NT, Max).

showWrongMenuItemError:- nl, writeTabnl("Wrong menu item").
showMainMenuAlert:- nl, writeTabnl("Press enter to go to the main menu"), get_single_char(_).

logp([]):-!, get_single_char(_), nl.
logp([Message|T]):-
	write(Message),
	logp(T)
.
logp(Message):- not(is_list(Message)), logp([Message]).

log([]):-!, nl.
log([Message|T]):-
	write(Message),
	log(T)
.
log(Message):- not(is_list(Message)), log([Message]).

writeTab(Message):- write("    "), write(Message).

writeTab(0, Message):- !, write(Message).
writeTab(Counter, Message):- 
	write("    "), 
	CounterDec is Counter-1,
	writeTab(CounterDec, Message).

writeTabnl(Message):- writeTab(Message), nl.
writeTabnl(0, Message):- !, write(Message), nl.
writeTabnl(Counter, Message):- 
	write("    "), 
	CounterDec is Counter-1,
	writeTabnl(CounterDec, Message).

readPositive(Result, Default):-
	readString(String), 
	(number_string(Temp, String); String = "", Temp = Default),
	positiveCheck(Temp, Default),
	Result = Temp,
	!
.
readPositive(Result, Default):- writeTab("Type a correct number: "), readPositive(Result, Default).

positiveCheck(Temp, _):- Temp >= 0, !.
positiveCheck(Default, Default):-!.

readInt(Result):-
	readString(String), 
	number_string(Result, String)
.

readInt(Result, _):- readInt(Result).
readInt(Default, Default).

readTerm(Result):-
	readln(Read),
	atomics_to_string(Read, " ", Result)
.

readStringQuoted(Result):-
	read_line_to_string(current_input, Terms),
	term_string(Terms, Result)
.

readString(Result):- read_line_to_string(current_input, Result).

readStringIgnoreCase(Result):-
	read_line_to_string(current_input, String),
	string_lower(String, Result)
.

readSubMenuitem(MenuItem):-
	writeTab("Type a menu item: "),
	readStringIgnoreCase(MenuItem),
	nl
.

readMenuitem(MenuItem):-
	writeTab("Type a menu item: "),
	readStringIgnoreCase(MenuItem),
	nl,
	handleBaseMenuItem(MenuItem)
.

handleBaseMenuItem("exit"):-halt.
handleBaseMenuItem("halt"):-halt.
handleBaseMenuItem("menu"):-!, openMainMenu.
handleBaseMenuItem("help"):-!, openHelp.
handleBaseMenuItem(_).

unwrapPairs([], Stack, Stack).
unwrapPairs([pair(Id, _)|T], Stack, Result):-
	unwrapPairs(T, [Id|Stack], Temp),
	reverse(Temp, Result).

subString(String, SubString):- sub_string(String, _, _, _, SubString), !.

insert(Furniture):-
    append('database.pl'),
    nl, write(Furniture), write("."),
    told,
	consult("database.pl")
.

insert(Part):-
    append('database.pl'),
    nl, write(Part), write("."),
    told,
	consult("database.pl")
.

:- consult("database.pl"), openMainMenu.