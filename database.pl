:- dynamic furniture/5.
:- dynamic part/2.

part(0,"Nail",5).
part(1,"Table leg (black wood)",1000).
part(2,"Table top (black wood)",5000).
part(3,"Table top (red wood)",7000).
part(4,"Office Chair bottom",6000).
part(5,"Office Chair top",5000).
furniture(0,"Table Royal",[group(0,16),group(1,4),group(3,1)],20000,[param("radius","70cm"),param("height","75cm"),param("weight","18kg")]).
furniture(1,"Table Black",[group(0,12),group(1,4),group(2,1)],15000,[param("width","70cm"),param("length","120cm"),param("height","75cm"),param("weight","15kg")]).
furniture(2,"Office Chair",[group(0,8),group(4,1),group(5,1)],17000,[param("height","120cm"),param("weight","10kg")]).
furniture(3,"Test",[group(1,2),group(0,3)],999,[]).
furniture(4,"Mega Chair",[group(5,1),group(4,1),group(0,20)],12000,[]).
furniture(5,"Test",[group(0,4)],1111,[param("test","qwe")]).
furniture(6,"Closet",[group(1,6),group(0,4)],10000,[param("height","200cm"),param("length","200cm"),param("width","50cm")]).